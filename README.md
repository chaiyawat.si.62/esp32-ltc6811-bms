# ESP32 LTC6811 BMS

A battery managment system using A esp32 brain talking to 1 to n LTC6811's using ISO bus daisy chaining.

Has CAN and RS485 interfaces. 



## Work in progress

![WIP](WiP.jpg)

## Hardware

![Render](/Hardware/Render.PNG)



V0.1
Needs reset and program button's 
Remove 3.3V from programming headder.



## Firmware

![Interface](Software/ESP32BMS/Website/WebInterface.png)

I want to change from arduino core to Free RTOS. 



## FAQ

Why can i not talk to the LTC6811?
have you applied vbat? 

Why do my registers keep resetting?
has the LTC6811 gone to sleep from 2 sec on inactivity?
